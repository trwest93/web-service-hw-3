class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.number :job_id
      t.string :job_name
      t.date :due_date
      t.string :location
      t.number :emp_id

      t.timestamps
    end
  end
end
